.. vorlage documentation master file, created by
   sphinx-quickstart on Sat Feb 10 14:42:24 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to vorlage's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. doxygenclass:: nola::static_array
  :project: zell
  :members: create
  :private-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
