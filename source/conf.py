# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "vorlage"
copyright = "2024, Jayesh Badwaik"
author = "Jayesh Badwaik"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# extensions = ["sphinx_rtd_theme"]
import sys

extensions = ["sphinx_multiversion", "sphinx_rtd_theme", "breathe"]

breathe_projects = {
  "zell": "./xml"
}

templates_path = ["_templates"]
exclude_patterns = []

html_sidebars = {
    "**": ["about.html", "navigation.html", "relations.html", "searchbox.html", "versioning.html"]
}



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]


## Whitelist pattern for tags (set to None to ignore all tags)
#smv_tag_whitelist = r"^.*$"
#
## Whitelist pattern for branches (set to None to ignore all branches)
smv_branch_whitelist = r"main|mr/.*"
#
# Whitelist pattern for remotes (set to None to use local branches only)
smv_remote_whitelist = None
#
## Pattern for released versions
smv_released_pattern = r"(^refs/tags/[0-9]*\.[0-9]*\.[0-9]*$)"
#smv_released_pattern = r'^.*$'
#
## Format for versioned output directories inside the build directory
#smv_outputdir_format = "{ref.name}"
#
## Determines whether remote or local git branches/tags are preferred if their output dirs conflict
smv_prefer_remote_refs = False

smv_latest_version = "0.0.2"


html_context = {
}
