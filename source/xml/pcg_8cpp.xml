<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.10.0" xml:lang="en-US">
  <compounddef id="pcg_8cpp" kind="file" language="C++">
    <compoundname>pcg.cpp</compoundname>
    <includes local="no">cstdint</includes>
    <includes refid="pcg_8hpp" local="no">testzell/pcg.hpp</includes>
    <incdepgraph>
      <node id="3">
        <label>testzell/pcg.hpp</label>
        <link refid="pcg_8hpp"/>
        <childnode refid="2" relation="include">
        </childnode>
      </node>
      <node id="1">
        <label>test/common/cpp/lib/testzell/pcg.cpp</label>
        <link refid="pcg_8cpp"/>
        <childnode refid="2" relation="include">
        </childnode>
        <childnode refid="3" relation="include">
        </childnode>
      </node>
      <node id="2">
        <label>cstdint</label>
      </node>
    </incdepgraph>
    <innernamespace refid="namespacetestzell">testzell</innernamespace>
    <innernamespace refid="namespacetestzell_1_1pcg">testzell::pcg</innernamespace>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">//<sp/>-------------------------------------------------------------------------------------------------</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="2"><highlight class="normal"></highlight><highlight class="comment">//<sp/>SPDX-License-Identifier:<sp/>AGPL-3.0-only</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="3"><highlight class="normal"></highlight><highlight class="comment">//<sp/>SPDX-FileCopyrightText:<sp/>(C)<sp/>2022<sp/>Jayesh<sp/>Badwaik<sp/>&lt;j.badwaik@fz-juelich.de&gt;</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="4"><highlight class="normal"></highlight><highlight class="comment">//<sp/>-------------------------------------------------------------------------------------------------</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="5"><highlight class="normal"></highlight></codeline>
<codeline lineno="6"><highlight class="normal"></highlight><highlight class="preprocessor">#include<sp/>&lt;cstdint&gt;</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="7"><highlight class="normal"></highlight><highlight class="preprocessor">#include<sp/>&lt;<ref refid="pcg_8hpp" kindref="compound">testzell/pcg.hpp</ref>&gt;</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="8"><highlight class="normal"></highlight></codeline>
<codeline lineno="9"><highlight class="normal"></highlight><highlight class="keyword">namespace<sp/></highlight><highlight class="normal"><ref refid="namespacetestzell_1_1pcg" kindref="compound">testzell::pcg</ref><sp/>{</highlight></codeline>
<codeline lineno="10" refid="namespacetestzell_1_1pcg_1a6b1eb0aee3cd69a245a59f5a2395de4d" refkind="member"><highlight class="normal"></highlight><highlight class="keyword">auto</highlight><highlight class="normal"><sp/><ref refid="namespacetestzell_1_1pcg_1a6b1eb0aee3cd69a245a59f5a2395de4d" kindref="member">pcg32_random</ref>(<ref refid="structtestzell_1_1pcg_1_1state32" kindref="compound">state32</ref>*<sp/>rng)<sp/>-&gt;<sp/>uint32_t</highlight></codeline>
<codeline lineno="11"><highlight class="normal">{</highlight></codeline>
<codeline lineno="12"><highlight class="normal"><sp/><sp/></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp/>uint64_t<sp/>old<sp/>=<sp/>rng-&gt;state;</highlight></codeline>
<codeline lineno="13"><highlight class="normal"><sp/><sp/></highlight><highlight class="comment">//<sp/>Advance<sp/>internal<sp/>state</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="14"><highlight class="normal"><sp/><sp/>rng-&gt;state<sp/>=<sp/>(rng-&gt;state)<sp/>*<sp/>0X5851F42D4C957F2DULL;</highlight></codeline>
<codeline lineno="15"><highlight class="normal"><sp/><sp/></highlight><highlight class="comment">//<sp/>NOLINTNEXTLINE(hicpp-signed-bitwise)</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="16"><highlight class="normal"><sp/><sp/>rng-&gt;state<sp/>+=<sp/>(rng-&gt;stream<sp/>|<sp/>1);</highlight></codeline>
<codeline lineno="17"><highlight class="normal"><sp/><sp/></highlight><highlight class="keyword">auto</highlight><highlight class="normal"><sp/></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp/>xorshifted<sp/>=<sp/></highlight><highlight class="keyword">static_cast&lt;</highlight><highlight class="normal">uint32_t</highlight><highlight class="keyword">&gt;</highlight><highlight class="normal">(((old<sp/>&gt;&gt;<sp/>18U)<sp/>^<sp/>old)<sp/>&gt;&gt;<sp/>27U);</highlight></codeline>
<codeline lineno="18"><highlight class="normal"><sp/><sp/></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp/>uint32_t<sp/>rot<sp/>=<sp/>old<sp/>&gt;&gt;<sp/>59U;</highlight></codeline>
<codeline lineno="19"><highlight class="normal"><sp/><sp/></highlight><highlight class="comment">//<sp/>NOLINTNEXTLINE(hicpp-signed-bitwise)</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="20"><highlight class="normal"><sp/><sp/></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp/>(xorshifted<sp/>&gt;&gt;<sp/>rot)<sp/>|<sp/>(xorshifted<sp/>&lt;&lt;<sp/>((-rot)<sp/>&amp;<sp/>31));</highlight></codeline>
<codeline lineno="21"><highlight class="normal">}</highlight></codeline>
<codeline lineno="22"><highlight class="normal"></highlight></codeline>
<codeline lineno="23" refid="namespacetestzell_1_1pcg_1a569673ea5dac5b04bf2d1525fa60b540" refkind="member"><highlight class="normal"></highlight><highlight class="keyword">auto</highlight><highlight class="normal"><sp/><ref refid="namespacetestzell_1_1pcg_1a569673ea5dac5b04bf2d1525fa60b540" kindref="member">pc_seed_rng</ref>(<ref refid="structtestzell_1_1pcg_1_1state32" kindref="compound">state32</ref>*<sp/>rng,<sp/>uint64_t<sp/>seed,<sp/>uint64_t<sp/>stream)<sp/>-&gt;<sp/></highlight><highlight class="keywordtype">int</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="24"><highlight class="normal">{</highlight></codeline>
<codeline lineno="25"><highlight class="normal"><sp/><sp/>rng-&gt;stream<sp/>=<sp/>stream<sp/>*<sp/>2<sp/>+<sp/>1;</highlight></codeline>
<codeline lineno="26"><highlight class="normal"><sp/><sp/>rng-&gt;state<sp/>=<sp/>0;</highlight></codeline>
<codeline lineno="27"><highlight class="normal"><sp/><sp/><ref refid="namespacetestzell_1_1pcg_1a6b1eb0aee3cd69a245a59f5a2395de4d" kindref="member">pcg32_random</ref>(rng);</highlight></codeline>
<codeline lineno="28"><highlight class="normal"><sp/><sp/>rng-&gt;state<sp/>+=<sp/>seed;</highlight></codeline>
<codeline lineno="29"><highlight class="normal"><sp/><sp/><ref refid="namespacetestzell_1_1pcg_1a6b1eb0aee3cd69a245a59f5a2395de4d" kindref="member">pcg32_random</ref>(rng);</highlight></codeline>
<codeline lineno="30"><highlight class="normal"><sp/><sp/></highlight><highlight class="comment">//<sp/>Improve<sp/>quality<sp/>of<sp/>first<sp/>random<sp/>numbers</highlight><highlight class="normal"></highlight></codeline>
<codeline lineno="31"><highlight class="normal"><sp/><sp/><ref refid="namespacetestzell_1_1pcg_1a6b1eb0aee3cd69a245a59f5a2395de4d" kindref="member">pcg32_random</ref>(rng);</highlight></codeline>
<codeline lineno="32"><highlight class="normal"><sp/><sp/></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp/>0;</highlight></codeline>
<codeline lineno="33"><highlight class="normal">}</highlight></codeline>
<codeline lineno="34"><highlight class="normal">}<sp/></highlight><highlight class="comment">//<sp/>namespace<sp/>testzell::pcg</highlight><highlight class="normal"></highlight></codeline>
    </programlisting>
    <location file="test/common/cpp/lib/testzell/pcg.cpp"/>
  </compounddef>
</doxygen>
